@extends('adminlte.master')
@section('title','Create Pertanyaan')
@section('content')
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
    <div class="mx-5">
        <div class="card card-primary mt-3">
            <div class="card-header">
                <h3 class="card-title">Create Pertanyaan</h3>
            </div>
            <!-- form start -->
            <form role="form" action="/store" method="POST">
                @csrf
                <div class="card-body mt-5">
                    <div class="form-group">
                        <label for="judul">Judul</label>
                        <input type="text" class="form-control @error('judul') is-invalid @enderror" id="judul" name="judul" placeholder="Masukkan Judul">
                        @error('judul') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label for="pertanyaan">Pertanyaan</label>
                        <input type="text" class="form-control @error('isi') is-invalid @enderror" id="pertanyaan" name="isi" placeholder="Masukkan Pertanyaan">
                        @error('isi') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="ml-4">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <a class="btn btn-warning text-white ml-1" href="/pertanyaan">Cancel</a>
                </div>
            </form>
        </div>
    </div>
</body>

</html>
@endsection