@extends('adminlte.master')
@section('title','List Pertanyaan')
@section('content')
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
</head>

<body>
    <div class="container mt-3">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">List Pertanyaan</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <a class="btn btn-danger" href="/pertanyaan/create">Tambah Data</a>
                <hr>
                @if(session('status'))
                <div class="alert alert-success">
                    {{session('status')}}
                </div>
                @endif
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th style="width: 20px;">No</th>
                            <th style="width: 260px;">Judul</th>
                            <th>Pertanyaan</th>
                            <th style="width: 200px;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $row)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$row->judul}}</td>
                            <td>{{$row->isi}}</td>

                            <td>
                                <a href="/pertanyaan/{{$row->id}}" class="btn btn-info">Show</a>
                                <a href="/pertanyaan/{{$row->id}}/edit" class="btn btn-success ml-2">Edit</a>
                                <form action="/destroy/{{$row->id}}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
    </div>

    @push('scripts')
    <script>
        $(function() {
            $("#example1").DataTable();
        });
    </script>
    @endpush

</body>

</html>
@endsection