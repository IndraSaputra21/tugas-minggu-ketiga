@extends('adminlte.master')
@section('title','Show Pertanyaan')
@section('content')
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>
    <br>
    <div class="mx-5">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Show Pertanyaan</h3>
            </div>
            <!-- card start -->

            <div class="card-body">
                <h2 class="card-title">{{$data->judul}}</h2><br>
                <hr>
                <p class="card-text">{{$data->isi}}</p><br><br>
                <a href="/pertanyaan/create" class="btn btn-primary">Back to Create</a>
                <a href="/pertanyaan" class="btn btn-danger">Back to Index</a>
            </div>

        </div>
    </div>
</body>

</html>
@endsection